/*
 * CStack_test.cpp
 *
 *  Created on: 27 de mar de 2016
 *      Author: maiko
 */

#include "CStack.h"

#include "rpn.h"

using namespace rpn;

void CStack_test() {
	CStack *s = new CStack(4);

	s->push(CComplex(10, 1));
	ASSERT(s->size() == 1);
	s->push(CComplex(20, 1));
	ASSERT(s->size() == 2);
	s->push(CComplex(30, 1));
	ASSERT(s->size() == 3);
	s->push(CComplex(40, 1));
	ASSERT(s->size() == 4);
	ASSERT(s->isFull());
	s->push(CComplex(50, 1));
	ASSERT(s->size() == 4);
	s->pop();
	ASSERT(s->size() == 3);
	ASSERT(!s->isFull());
	s->pop();
	ASSERT(s->size() == 2);
	s->pop();
	ASSERT(s->size() == 1);
	s->pop();
	ASSERT(s->empty());
	s->pop();
	s->push(CComplex(10, 1));
	ASSERT(s->size() == 1);
}

