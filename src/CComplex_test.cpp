#include "CComplex.h"

#include "rpn.h"

using namespace rpn;

void CComplex_test() {
  CComplex ret = CComplex(5, 2) + CComplex(3, -7);
  ASSERT(ret == CComplex(8, -5));

  ret = CComplex(5, 2) - CComplex(3, -7);
  ASSERT(ret == CComplex(2, 9));

  ret = CComplex(1, -3) * CComplex(2, 5);
  ASSERT(ret == CComplex(17, -1));

  ret = CComplex(6, 3) / CComplex(7, -5);
  ASSERT(ret == CComplex(0.36486486486486486, 0.68918918918918914));

  ret = CComplex(10, 0) + CComplex(20, 0);
  ASSERT(ret == CComplex(30, 0));

  ret = CComplex(10, 0) - CComplex(20, 0);
  ASSERT(ret == CComplex(-10, 0));

  ret = CComplex(10, 0) * CComplex(20, 0);
  ASSERT(ret == CComplex(200, 0));

  ret = CComplex(10, 0) / CComplex(20, 0);
  ASSERT(ret == CComplex(0.5, 0));

  ret = CComplex(0, 10) + CComplex(0, 20);
  ASSERT(ret == CComplex(0, 30));

  ret = CComplex(0, 10) - CComplex(0, 20);
  ASSERT(ret == CComplex(0, -10));

  ret = CComplex(0, 10) * CComplex(0, 20);
  ASSERT(ret == CComplex(-200, 0));

  ret = CComplex(0, 10) / CComplex(0, 20);
  ASSERT(ret == CComplex(0.5, 0));

  ret = CComplex(10, 10)++;
  ASSERT(ret == CComplex(10, 10));

  ret = ++CComplex(10, 10);
  ASSERT(ret == CComplex(11, 11));

  ret = CComplex(10, 10);
  ret += ret;
  ASSERT(ret == CComplex(20, 20));

  ret = CComplex(10, 10);
  ret -= ret;
  ASSERT(ret == CComplex(0, 0));

  ret = CComplex(10, 10);
  ret *= ret;
  ASSERT(ret == CComplex(0, 200));

  ret = CComplex(10, 10);
  ret /= ret;
  ASSERT(ret == CComplex(1, 0));

}
