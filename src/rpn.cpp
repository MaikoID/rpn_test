/*
 * rpn.cpp
 *
 *  Created on: 27 de mar de 2016
 *      Author: maiko
 */

#include "rpn.h"

cute::suite make_suite(){
	cute::suite s;
	s.push_back(CUTE(CStack_test));
	s.push_back(CUTE(CComplex_test));
	s.push_back(CUTE(CCalculate_test_real));
	s.push_back(CUTE(CCalculate_test_complex));
	return s;
}

