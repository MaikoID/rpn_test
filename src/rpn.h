#include "cute_suite.h"
#include "cute.h"


extern void CCalculate_test_real();
extern void CCalculate_test_complex();
extern void CStack_test();
extern void CComplex_test();

extern cute::suite make_suite();
