/*
 * RPN_test.cpp
 *
 *  Created on: Mar 7, 2016
 *      Author: maiko.costa
 */

#include "CCalculate.h"

#include "rpn.h"

#include <iostream>

using namespace rpn;

void CCalculate_test_real() {
	CCalculate rpn;

	rpn.SaveOperand(CComplex(20.0, 0));
	ASSERT(rpn.size() == 1);

	rpn.SaveOperand(CComplex(10.0, 0));
	ASSERT(rpn.size() == 2);

	rpn.SaveOperand(CComplex(1.5, 0));
	ASSERT(rpn.size() == 3);

	rpn.SaveOperand(CComplex(2.5, 0));
	ASSERT(rpn.size() == 4);

	rpn.Calculate(static_cast<CCalculate::Operations>('+'));
	ASSERT(rpn.size() == 3);
	ASSERT(rpn.get() == CComplex(4, 0));

	rpn.Calculate(static_cast<CCalculate::Operations>('*'));
	ASSERT(rpn.size() == 2);
	ASSERT(rpn.get() == CComplex(40, 0));

	rpn.Calculate(static_cast<CCalculate::Operations>('/'));
	ASSERT(rpn.size() == 1);
	ASSERT(rpn.get() == CComplex(0.5, 0));
}

void CCalculate_test_complex() {
  CCalculate rpn;
  rpn.SaveOperand(CComplex(5, 2));
    ASSERT(rpn.size() == 1);
    ASSERT(rpn.get() == CComplex(5, 2));

    rpn.SaveOperand(CComplex(3, -7));
    ASSERT(rpn.size() == 2);
    rpn.Calculate(static_cast<CCalculate::Operations>('+'));
    ASSERT(rpn.size() == 1);
    ASSERT(rpn.get() == CComplex(8, -5));
}
